# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import calendar
from decimal import Decimal
from trytond.pool import Pool
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, If
# from trytond.report import Report
from sql import Null, Literal, With
from sql.aggregate import Sum, Min
from sql.conditionals import Case
from trytond.transaction import Transaction
from trytond.i18n import lazy_gettext
from itertools import tee, zip_longest
from dateutil.relativedelta import relativedelta
from sql.functions import CurrentTimestamp, DateTrunc, Extract, Trunc
from sql.operators import Concat, Between
from .goal import KIND, TYPE

_ZERO = Decimal(0)


def pairwise(iterable):
    a, b = tee(iterable)
    next(b)
    return zip_longest(a, b)


class Abstract(ModelSQL):

    company = fields.Many2One(
        'company.company', lazy_gettext("sale_goal.msg_goal_reporting_company"))
    quantity = fields.Integer(lazy_gettext("sale_goal.msg_goal_reporting_number"),
        help=lazy_gettext("sale_goal.msg_goal_reporting_number_help"))
    amount_target = fields.Numeric(
        lazy_gettext("sale_goal.msg_goal_reporting_amount_target"),
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    amount_achieved = fields.Numeric(
        lazy_gettext("sale_goal.msg_goal_reporting_amount_achieved"),
        digits=(16, Eval('currency_digits', 2)),
        depends=['currency_digits'])
    # percentage = fields.Numeric(
    #     lazy_gettext("sale_goal.msg_goal_reporting_percentage"),
    #     digits=(16, Eval('currency_digits', 2)),
    #     depends=['currency_digits'])

    time_series = None

    currency = fields.Function(fields.Many2One(
            'currency.currency',
            lazy_gettext("sale.msg_sale_reporting_currency")),
        'get_currency')
    currency_digits = fields.Function(
        fields.Integer(
            lazy_gettext("sale.msg_sale_reporting_currency_digits")),
        'get_currency_digits')

    # @classmethod
    # def _column_amount_achieved(cls, tables, withs, sign):
    #     move = tables['move']
    #     currency = withs['currency_rate']
    #     currency_company = withs['currency_rate_company']
    #     return Case(Sum(
    #         sign * cls.revenue.sql_cast(move.quantity) * move.unit_price
    #         * currency_company.rate / currency.rate))

    @property
    def time_series_all(self):
        delta = self._period_delta()
        for ts, next_ts in pairwise(self.time_series or []):
            yield ts
            if delta and next_ts:
                date = ts.date + delta
                while date < next_ts.date:
                    yield None
                    date += delta

    @classmethod
    def _period_delta(cls):
        context = Transaction().context
        return {
            'year': relativedelta(years=1),
            'month': relativedelta(months=1),
            'day': relativedelta(days=1),
            }.get(context.get('period'))

    # def get_trend(self, name):
    #     name = name[:-len('_trend')]
    #     if pygal:
    #         chart = pygal.Line()
    #         chart.add('', [getattr(ts, name) if ts else 0
    #                 for ts in self.time_series_all])
    #         return chart.render_sparktext()

    def get_currency(self, name):
        return self.company.currency.id

    def get_currency_digits(self, name):
        return self.company.currency.digits


class AbstractTimeseries(Abstract):

    date = fields.Date("Date")

    @classmethod
    def __setup__(cls):
        super(AbstractTimeseries, cls).__setup__()
        cls._order = [('date', 'ASC')]

    @classmethod
    def _columns(cls, tables, withs):
        return super(AbstractTimeseries, cls)._columns(tables, withs) + [
            cls._column_date(tables, withs).as_('date')]

    @classmethod
    def _column_date(cls, tables, withs):
        context = Transaction().context
        move = tables['line.move']
        date = DateTrunc(context.get('period'), move.date)
        date = cls.date.sql_cast(date)
        return date

    @classmethod
    def _group_by(cls, tables, withs):
        return super(AbstractTimeseries, cls)._group_by(tables, withs) + [
            cls._column_date(tables, withs)]


class Context(ModelView):
    "Goal Reporting Context"
    __name__ = 'goal.reporting.context'

    company = fields.Many2One('company.company', "Company", required=True)
    from_date = fields.Date("From Date",
        domain=[
            If(Eval('to_date') & Eval('from_date'),
                ('from_date', '<=', Eval('to_date')),
                ()),
            ],
        depends=['to_date'])
    to_date = fields.Date("To Date",
        domain=[
            If(Eval('from_date') & Eval('to_date'),
                ('to_date', '>=', Eval('from_date')),
                ()),
            ],
        depends=['from_date'])
    period = fields.Selection([
            ('', ''),
            ('year', "Year"),
            ('month', "Month"),
            ('day', "Day"),
            ], "Period", required=True)
    type = fields.Selection(TYPE, 'Type', required=True)

    kind = fields.Selection(KIND, 'kind', states={
        'required': True
        })
    with_tax = fields.Boolean('With Tax')

    @classmethod
    def default_company(cls):
        return Transaction().context.get('company')

    @classmethod
    def default_type(cls):
        return 'monthly'

    @classmethod
    def default_kind(cls):
        return 'salesman'

    @classmethod
    def default_from_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        context = Transaction().context
        if 'from_date' in context:
            return context['from_date']
        return Date.today() - relativedelta(years=1)

    @classmethod
    def default_to_date(cls):
        pool = Pool()
        Date = pool.get('ir.date')
        context = Transaction().context
        if 'to_date' in context:
            return context['to_date']
        return Date.today()

    @classmethod
    def default_period(cls):
        return Transaction().context.get('period', 'month')


class GoalIndicatorMixin(object):
    __slots__ = ()
    indicator = fields.Many2One(
        'sale.indicator', "Indicator",
        context={
            'company': Eval('company', -1),
            },
        depends=['company'])
    year = fields.Numeric('Year')
    period = fields.Numeric('Period')
    year_period = fields.Char('Year Period')

    @classmethod
    def table_query(cls):
        from_item, tables, withs = cls._joins()

        return from_item.select(*cls._columns(tables, withs),
            where=cls._where(tables, withs),
            group_by=cls._group_by(tables, withs),
            with_=withs.values())

    @classmethod
    def _joins(cls):
        pool = Pool()
        Indicator = pool.get('sale.indicator')
        GoalLine = pool.get('sale.goal.line')
        Company = pool.get('company.company')
        Currency = pool.get('currency.currency')
        Line = pool.get('account.invoice.line')
        Invoice = pool.get('account.invoice')
        # Goal = pool.get('sale.goal')
        Product = pool.get('product.product')
        TemplateCategory = pool.get('product.template-product.category.all')
        tables = {}
        tables['sale.indicator'] = indicator = Indicator.__table__()
        tables['sale.goal.line'] = goal_line = GoalLine.__table__()
        # tables['sale.goal'] = goal = Goal.__table__()
        tables['line'] = line = Line.__table__()
        tables['line.invoice'] = invoice = Invoice.__table__()
        tables['line.invoice.company'] = company = Company.__table__()
        tables['line.product'] = product = Product.__table__()
        tables['line.product.template_category'] = template_category = TemplateCategory.__table__()

        withs = {}
        currency_invoice = With(query=Currency.currency_rate_sql())
        withs['currency_invoice'] = currency_invoice
        currency_company = With(query=Currency.currency_rate_sql())
        withs['currency_company'] = currency_company

        context = Transaction().context
        condition_between = Between(invoice.invoice_date, goal_line.start_date, goal_line.end_date)

        kind = context['kind']
        from_item = (goal_line.join(indicator, condition=goal_line.id == indicator.goal_line))

        if kind == 'salesman':
            from_item = (from_item
                .join(invoice, condition=(indicator.salesman == invoice.salesman) & (condition_between))
                .join(line, condition=invoice.id == line.invoice))
        elif kind == 'category':
            from_item = (from_item
                .join(template_category, condition=indicator.category == template_category.id)
                .join(product, condition=product.template == template_category.template)
                .join(line, condition=product.id == line.product)
                .join(invoice, condition=((line.invoice == invoice.id) & condition_between)))
        elif kind == 'product':
            from_item = (from_item
                .join(line, condition=indicator.product == line.product)
                .join(invoice, condition=(line.invoice == invoice.id)
                & condition_between))

        from_item=(from_item.join(currency_invoice,
                condition=(invoice.currency == currency_invoice.currency)
                & (currency_invoice.start_date <= invoice.invoice_date)
                & ((currency_invoice.end_date == Null)
                    | (currency_invoice.end_date >= invoice.invoice_date)))
            .join(company, condition=invoice.company == company.id)
            .join(currency_company,
                condition=(company.currency == currency_company.currency)
                & (currency_company.start_date <= invoice.invoice_date)
                & ((currency_company.end_date == Null)
                    | (currency_company.end_date >= invoice.invoice_date))
                ))
        return from_item, tables, withs

    @classmethod
    def _columns(cls, tables, withs):
        line = tables['line']
        invoice = tables['line.invoice']
        indicator = tables['sale.indicator']
        currency_company = withs['currency_company']
        currency_invoice = withs['currency_invoice']
        type_year_period = cls.year_period.sql_type().base

        quantity = line.quantity
        amount_achieved = cls.amount_achieved.sql_cast(
            Sum(quantity * line.unit_price
                * currency_company.rate / currency_invoice.rate))
        return [
            # cls._column_id(tables, withs).as_('id'),
            indicator.id.as_('id'),
            indicator.id.as_('indicator'),
            Literal(0).as_('create_uid'),
            CurrentTimestamp().as_('create_date'),
            cls.write_uid.sql_cast(Literal(Null)).as_('write_uid'),
            cls.write_date.sql_cast(Literal(Null)).as_('write_date'),
            invoice.company.as_('company'),
            Extract('YEAR', invoice.invoice_date).as_('year'),
            cls._column_date(tables, withs).as_('period'),
            Concat(Extract('YEAR', invoice.invoice_date).cast(type_year_period),
            cls._column_date(tables, withs).cast(type_year_period)).as_('year_period'),
            indicator.amount.as_('amount_target'),
            amount_achieved.as_('amount_achieved'),
            Sum(quantity).as_('quantity'),
            ]

    @classmethod
    def _where(cls, tables, withs):
        context = Transaction().context
        invoice = tables['line.invoice']
        goal_line = tables['sale.goal.line']

        type = context.get('type')
        where = invoice.company == context.get('company')
        where &= invoice.state.in_(cls._invoice_states())
        where &= goal_line.type == type
        from_date = context.get('from_date')

        if from_date:
            where &= goal_line.start_date >= from_date
            where &= invoice.invoice_date >= from_date
        to_date = context.get('to_date')
        if to_date:
            where &= goal_line.end_date <= to_date
            where &= invoice.invoice_date <= to_date
        return where

    @classmethod
    def _column_date(cls, tables, withs):
        context = Transaction().context
        invoice = tables['line.invoice']
        type = context.get('type')
        if type == 'monthly':
            date = Extract('MONTH', invoice.invoice_date)
        elif type == 'bimonthly':
            date = Trunc((Extract('MONTH', invoice.date)+1)/2)
        elif type == 'quarterly':
            date = Extract('QUARTER', invoice.date),
        elif type == 'annual':
            date = Extract('YEAR', invoice.date),

        date = cls.period.sql_cast(date)
        return date

    @classmethod
    def _group_by(cls, tables, withs):
        indicator = tables['sale.indicator']
        invoice = tables['line.invoice']
        type_year_period = cls.year_period.sql_type().base
        group_by = [
            indicator.id,
            Extract('YEAR', invoice.invoice_date),
            cls._column_date(tables, withs),
            indicator.amount,
            invoice.company,
            Concat(Extract('YEAR', invoice.invoice_date).cast(type_year_period), cls._column_date(tables, withs).cast(type_year_period))
        ]
        return group_by

    @classmethod
    def _invoice_states(cls):
        return ['posted', 'paid', 'validated']

    # def get_rec_name(self, name):
    #     return self.indicator.rec_name


class GoalIndicator(GoalIndicatorMixin, Abstract, ModelView):
    "Goal Reporting per Indicator"
    __name__ = 'goal.reporting.indicator'

    # time_series = fields.One2Many(
    #     'goal.reporting.indicator.time_series', 'Indicator', "Time Series")

    @classmethod
    def __setup__(cls):
        super().__setup__()
        # cls._order.insert(0, ('indicator', 'ASC'))

    @classmethod
    def _column_id(cls, tables, withs):
        indicator = tables['sale.indicator']
        return indicator.id
