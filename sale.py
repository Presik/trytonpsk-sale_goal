# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from datetime import date
from trytond.pool import Pool
from trytond.model import ModelView, fields
from trytond.pyson import Eval
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.report import Report
from trytond.transaction import Transaction
import datetime

_STATES = {
    'readonly': Eval('state') != 'draft',
}

_ZERO = Decimal(0)


class SaleDailyStart(ModelView):
    'Sale Daily Start'
    __name__ = 'sale_goal.sale_daily.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    sale_date = fields.Date('Sale Date', required=True)
    journal = fields.Many2One('account.statement.journal', 'Journal')
    include_canceled = fields.Boolean('Include Canceled')

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_sale_date():
        Date = Pool().get('ir.date')
        return Date.today()


class SaleDaily(Wizard):
    'Sale Daily'
    __name__ = 'sale_goal.sale_daily'
    start = StateView('sale_goal.sale_daily.start',
        'sale_goal.sale_daily_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('sale_goal.sale_daily_report')

    def do_print_(self, action):
        journal_id = None
        if self.start.journal:
            journal_id = self.start.journal.id
        report_context = {
            'company': self.start.company.id,
            'sale_date': self.start.sale_date,
            'journal': journal_id,
            'include_canceled': self.start.include_canceled,
            }
        return action, report_context

    def transition_print_(self):
        return 'end'


class SaleDailyReport(Report):
    'Sale Daily Report'
    __name__ = 'sale_goal.sale_daily_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Sale = pool.get('sale.sale')
        Company = pool.get('company.company')
        company = Company(data['company'])
        Journal = pool.get('account.statement.journal')

        dom_sales = [
            ('company', '=', data['company']),
            ('sale_date', '=', data['sale_date']),
            ('number', '!=', None),
        ]
        states_sale = ['confirmed', 'processing', 'done']
        if data['include_canceled']:
            states_sale.append('cancel')
        dom_sales.append(('state', 'in', states_sale))

        journal_name = ''
        if data['journal']:
            dom_sales.append(('payments.statement.journal', '=', data['journal']))
            journal = Journal(data['journal'])
            journal_name = journal.name.upper()
        sales = Sale.search(dom_sales, order=[('number', 'ASC')])

        untaxed_amount_cash = []
        untaxed_amount_credit = []
        tax_amount_ = []
        total_amount_ = []
        total_amount_ = []
        total_journal_paid_ = []
        sequence = 0
        sales_rec = []
        for sale in sales:
            sequence += 1
            journal_paid = _ZERO
            if data['journal'] and sale.state != 'cancel':
                for p in sale.payments:
                    if p.statement.journal.id == data['journal']:
                        journal_paid += p.amount
                total_journal_paid_.append(journal_paid)

            if sale.payment_term.payment_type == '1':
                untaxed_amount_cash.append(sale.untaxed_amount)
            else:
                untaxed_amount_credit.append(sale.untaxed_amount)
            sale_device = None
            if hasattr(sale, 'sale_device'):
                sale_device = sale.sale_device
            if sale.state == 'cancel':
                continue
            sales_rec.append({'sale': sale, 'sequence': sequence, 'journal_paid': journal_paid, 'sale_device': sale_device})
            # untaxed_amount_.append(sale.untaxed_amount)
            tax_amount_.append(sale.tax_amount)
            total_amount_.append(sale.total_amount)
        report_context['records'] = sales_rec
        report_context['total_amount'] = sum(total_amount_)
        report_context['tax_amount'] = sum(tax_amount_)
        report_context['untaxed_amount_cash'] = sum(untaxed_amount_cash)
        report_context['untaxed_amount_credit'] = sum(untaxed_amount_credit)
        report_context['date'] = data['sale_date']
        report_context['company'] = company.party.name
        report_context['journal'] = journal_name
        report_context['total_journal_paid'] = sum(total_journal_paid_)
        return report_context
