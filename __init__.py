# This file is part sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import goal
from . import goal_reporting
from . import sale


def register():
    Pool.register(
        goal.SaleIndicator,
        goal.Goal,
        goal.GoalLine,
        # goal.SaleGoalMonthRankingStart,
        # goal.SaleGoalAnnualRankingStart,
        # goal.SaleGoalJournalStart,
        sale.SaleDailyStart,
        goal_reporting.Context,
        goal_reporting.GoalIndicator,
        module='sale_goal', type_='model')
    Pool.register(
        # goal.SaleGoalMonthRanking,
        # goal.SaleGoalAnnualRanking,
        # goal.SaleGoalJournal,
        sale.SaleDaily,
        goal.SaleGoalAchievement,
        module='sale_goal', type_='wizard')
    Pool.register(
        goal.SaleGoalReport,
        # goal.SaleGoalJournalReport,
        # goal.SaleGoalMonthRankingReport,
        # goal.SaleGoalAnnualRankingReport,
        sale.SaleDailyReport,
        goal.SaleGoalAchievementReport,
        module='sale_goal', type_='report')
